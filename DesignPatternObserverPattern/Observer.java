package DesignPatternObserverPattern;

public interface Observer {

	public void update(String operation, String record);
}
