/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DesignPatterns.Flywieght;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Pelle
 */
//Flyweight is a factory(creational). It it used to conserve memory by sharing data. In this example we use PostalCode, we 
//on create a new postalCode for our Letters if it doesn't exist.
//GANG OF FOUR definition: "Facilitates the reuse of many fine grained objects,
//making the utilization of large numbers of objects more efficient"
//If the object doesn't exist - create new PostalCode object and save it in your map. Return object
//After the contains we return the object.
public class ZipCodeRegistry {
 
    private  Map<String, ZipCode> postalCodes = new HashMap();
    
    public ZipCode lookup(String postalCodeString) {
    
        if(!postalCodes.containsKey(postalCodeString)) 
            postalCodes.put(postalCodeString, new ZipCode(postalCodeString));
        
            return postalCodes.get(postalCodeString);
    }
    
    public int getMapSize() {
    
        return postalCodes.size();
    }
}
