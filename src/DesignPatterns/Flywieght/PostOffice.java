/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DesignPatterns.Flywieght;

import java.util.ArrayList;

/**
 *
 * @author Pelle
 */
public class PostOffice {

    private ZipCodeRegistry zipCodeRegistry = new ZipCodeRegistry();
    private ArrayList<Letter> letterList = new ArrayList();
    
    public void send(String postalCode, String address) {
    
        ZipCode newPostalCode = zipCodeRegistry.lookup(postalCode);
        Letter letter = new Letter(newPostalCode, address);
        letterList.add(letter);
        
    }
    
    public static void main(String[] args) {
        
        PostOffice postOffice = new PostOffice();
        
        postOffice.send("2300", "hej vej nr 9");
        postOffice.send("2300", "hej vej nr 9");
        postOffice.send("2450", "hej vej nr 9");
        postOffice.send("2300", "hej vej nr 9");
        postOffice.send("2300", "hej vej nr 9");
        postOffice.send("1337", "hej vej nr 9");
        postOffice.send("2300", "hej vej nr 9");
        postOffice.send("2300", "hej vej nr 9");
        postOffice.send("2300", "hej vej nr 9");
        postOffice.send("2300", "hej vej nr 9");
        postOffice.send("2300", "hej vej nr 9");
        
        System.out.println(postOffice.zipCodeRegistry.getMapSize());
        
    }
}
