/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DesignPatterns.Flywieght;

/**
 *
 * @author Pelle
 */
public class Letter {
   
    private final ZipCode zipCode;
    private final String address;
    
    public Letter(ZipCode zipCode, String address) {
    
        this.zipCode = zipCode;
        this.address = address;
    }
}
