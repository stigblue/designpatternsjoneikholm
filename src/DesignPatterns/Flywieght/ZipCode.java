/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DesignPatterns.Flywieght;

/**
 *
 * @author Pelle
 */
public class ZipCode {
 
    private final String postalCode;
    
    public ZipCode(String newPostalCode) {
    
        this.postalCode = newPostalCode;
    }

    @Override
    public String toString() {
        return "PostalCode{" + "postalCode=" + postalCode + '}';
    }
    
    
}
