package decorator;

// Denne klasse tilf�jer en BCC adresse til emailen som bliver vist f�r
// selve beskeden. Den indeholder en reference til mailen der bliver
// oprettet i main metoden i MailDecoratorTest.

public class BCC extends EmailDecorator {

    private Email email;

    public BCC(Email email) {
        this.email = email;
    }

    @Override
    public String message() {
        return "| BCC: senpai@plznoticeme.shark\n" + email.message();
    }

}
