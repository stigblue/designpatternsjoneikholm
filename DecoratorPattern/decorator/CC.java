package decorator;

// Denne klasse tilf�jer en CC adresse til emailen som bliver vist f�r
// selve beskeden. Den indeholder en reference til mailen der bliver
// oprettet i main metoden i MailDecoratorTest.

public class CC extends EmailDecorator {

    private Email email;

    public CC(Email email) {
        this.email = email;
    }

    @Override
    public String message() {
        return "| CC: batman@noparents.org\n" + email.message();
    }

}
