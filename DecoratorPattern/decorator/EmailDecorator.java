package decorator;

// Det generelle interface for de forskellige decorations. Alle
// elementer der skal kunne tilf�je til mailen skal extende
// denne klasse.

public abstract class EmailDecorator extends Email {

    public abstract String message();

}
