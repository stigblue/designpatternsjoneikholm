package decorator;

public class Email {

    // Emailens besked
    // Beskeden er hard codet ind for at illustrere princippet
    private String message;

    // Constructor til Email klassen
    // Mailens besked s�ttes her
    public Email() {
        message = "\n\t\"Hello! This is an email! Fear the terror!\"\n";
    }

    // Metode til at returnere mailens besked
    public String message() {
        return message;
    }

}
