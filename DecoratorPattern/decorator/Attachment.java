package decorator;

// Denne klasse tilf�jer et attachment til emailen som bliver vist efter
// selve beskeden. Den indeholder en reference til mailen der bliver
// oprettet i main metoden i MailDecoratorTest.

public class Attachment extends EmailDecorator {

    private Email email;

    public Attachment(Email email) {
        this.email = email;
    }

    @Override
    public String message() {
        return email.message() + "\n| Attachment: http://i.imgur.com/DEsYTI3.png";
    }
}
