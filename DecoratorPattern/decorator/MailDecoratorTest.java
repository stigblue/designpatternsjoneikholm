package decorator;

// Denne klasse indeholder main klassen til at demonstrere decorator pattern
//
// F�rst instantieres et Email objekt og herefter tilf�jes der et attachment,
// en CC adresse samt en BCC adresse. Det er muligt at tilf�je flere af samme
// type tilf�jelser til mailen
//
// Til sidst printes emailens indhold ud samt de tilf�jede elementer

public class MailDecoratorTest {

    public static void main(String[] args) {

        // Der oprettes en email som alene kun indeholder en enkelt String
        // som besked og intet andet
        Email email = new Email();

        // Vi kan tilf�je de forskellige elementer s�ledes, da alle klasserne
        // extender EmailDecorator som yderligere extender Email. Der sendes
        // en reference til 'email' med for hver is�r s�ledes at emailens
        // besked opdateres for hver.
        //
        // Disse fungerer alts� som wrappere p� det originale email objekt
        email = new Attachment(email);
        email = new CC(email);
        email = new BCC(email);
        email = new Attachment(email);

        // Skriv mailens samlede besked ud efter at have tilf�jet CC, BCC og
        // attachments.
        System.out.println("---------------------------------------------------\n" +

                email.message() +

                "\n---------------------------------------------------");

    }

}
